import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:testapp2/lib/screeens/signup_screen.dart';


import '../amplifyconfiguration.dart';
import 'home_screen.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});

  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> _configureAmplify() async {
    final auth = AmplifyAuthCognito();
    await Amplify.addPlugins(auth as List<AmplifyPluginInterface>);
    await Amplify.configure(amplifyconfig);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: const Text("Log In")),
      body: FutureBuilder<void>(
          key: _formKey,
          future: _configureAmplify(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return ListView(
                children: [
                  OutlinedAutomatedNextFocusableTextFormField(
                    controller: _phoneNumberController,
                    labelText: 'Phone Number',
                    inputType: TextInputType.phone,
                  ),
                  OutlinedAutomatedNextFocusableTextFormField(
                    controller: _passwordController,
                    obscureText: true,
                    labelText: 'Password',
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      onPressed: () async {
                        final phoneNumber = _phoneNumberController.text;
                        final password = _passwordController.text;
                        if (phoneNumber.isEmpty || password.isEmpty) {
                          debugPrint(
                              'One of the fields is empty. Not ready to submit.');
                        } else {
                          _signInUser(context);
                        }
                      },
                      child: const Text('Sign in'),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlinedButton(
                        child: const Text("Create New Account"),
                        onPressed: () => {_gotoSignUpScreen(context)}),
                  ),
                ],
              );
            }
            if (snapshot.hasError) {
              return Text('Some error happened: ${snapshot.error}');
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          }),
    );
  }

  Future<void> _signInUser(BuildContext context) async {
    final phoneNumber = _phoneNumberController.text;
    final password = _passwordController.text;
    final result = await Amplify.Auth.signIn(
      username: phoneNumber,
      password: password,
    );
    print(
        "phoneNumber: ${_phoneNumberController.text} Password: ${_passwordController.text}");
    try {
      if (result.isSignedIn) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (_) => HomeScreen()),
        );
      }
    } on AuthException catch (e) {
      safePrint('Error signing in: ${e.message}');
    }
  }

  void _gotoSignUpScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => SignUpScreen(),
      ),
    );
  }
}
