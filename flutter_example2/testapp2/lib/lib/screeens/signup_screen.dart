import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:flutter/material.dart';

import '../amplifyconfiguration.dart';
import 'home_screen.dart';

class SignUpScreen extends StatelessWidget {
  SignUpScreen({super.key});

  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _activationCodeController =
      TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> _configureAmplify() async {
    final auth = AmplifyAuthCognito();
    await Amplify.addPlugins(auth as List<AmplifyPluginInterface>);
    await Amplify.configure(amplifyconfig);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: const Text("Sign UP")),
      body: FutureBuilder<void>(
          key: _formKey,
          future: _configureAmplify(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return ListView(
                children: [
                  OutlinedAutomatedNextFocusableTextFormField(
                    controller: _phoneNumberController,
                    labelText: 'Phone Number',
                    inputType: TextInputType.phone,
                  ),
                  OutlinedAutomatedNextFocusableTextFormField(
                    controller: _passwordController,
                    obscureText: true,
                    labelText: 'Password',
                  ),
                  OutlinedAutomatedNextFocusableTextFormField(
                    controller: _emailController,
                    labelText: 'E-mail address',
                    inputType: TextInputType.emailAddress,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      onPressed: () async {
                        final phoneNumber = _phoneNumberController.text;
                        final password = _passwordController.text;
                        final email = _emailController.text;
                        if (phoneNumber.isEmpty ||
                            password.isEmpty ||
                            email.isEmpty) {
                          debugPrint(
                              'One of the fields is empty. Not ready to submit.');
                        } else {
                          _signUpUser(context);
                        }
                      },
                      child: const Text('Sign Up'),
                    ),
                  ),
                ],
              );
            }
            if (snapshot.hasError) {
              return Text('Some error happened: ${snapshot.error}');
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          }),
    );
  }

  Future<void> _signUpUser(BuildContext context) async {
    final phoneNumber = _phoneNumberController.text;
    final password = _passwordController.text;
    final email = _emailController.text;
    final result = await Amplify.Auth.signUp(
      username: phoneNumber,
      password: password,
      options: SignUpOptions(
        userAttributes: {
          CognitoUserAttributeKey.email: email,
          CognitoUserAttributeKey.phoneNumber: phoneNumber
        },
      ),
    );
    if (result.isSignUpComplete) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (_) => HomeScreen()),
      );
    } else {
      showDialog<void>(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('Confirm the user'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text('Check your phone number and enter the code below'),
              OutlinedAutomatedNextFocusableTextFormField(
                controller: _activationCodeController,
                padding: const EdgeInsets.only(top: 16),
                labelText: 'Activation Code',
                inputType: TextInputType.number,
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Dismiss'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            TextButton(
              child: const Text('Confirm'),
              onPressed: () {
                try {
                  Amplify.Auth.confirmSignUp(
                    username: phoneNumber,
                    confirmationCode: _activationCodeController.text,
                  ).then((result) async {
                    if (result.isSignUpComplete) {
                      // await Amplify.Auth.updateUserAttribute(
                      //     userAttributeKey: CognitoUserAttributeKey.email,
                      //     value: email);
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (_) => HomeScreen()),
                      );
                    }
                  });
                } on AuthException catch (e) {
                  print("Error${e.message}");
                }
              },
            ),
          ],
        ),
      );
    }
  }
}

class OutlinedAutomatedNextFocusableTextFormField extends StatelessWidget {
  const OutlinedAutomatedNextFocusableTextFormField({
    this.padding = const EdgeInsets.all(8),
    this.obscureText = false,
    this.labelText,
    this.controller,
    this.inputType,
    Key? key,
  }) : super(key: key);

  final String? labelText;
  final TextEditingController? controller;
  final TextInputType? inputType;
  final EdgeInsets padding;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: TextFormField(
        controller: controller,
        obscureText: obscureText,
        keyboardType: inputType,
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: labelText,
        ),
      ),
    );
  }
}
